# deployment

### 目录说明

 目录    |  说明
 --------|--------
 all-in-one | 单机部署所有实例
 azure | azure容器
 

### 部署


#### 单机docker部署

* 进入`all-in-one`目录
* 本地新建`.env`文件并修改配置，内容参考`.env.example`，主要是修改IP（域名），OPENAPI的KEY，以及MySQL服务器的连接信息
* 执行 `docker compose up -d` 启动

初次部署，要初始化数据库

* 使用`docker ps`找到django容器的ID，如`f2eb4b1d96b3`
* 进入容器的shell `docker exec -it f2eb4b1d96b3 "/bin/sh" `
* 执行 `python manage.py migrate` 和 `python manage.py createsuperuser`
* 访问数据库表，拿到token


#### Azure部署

* 